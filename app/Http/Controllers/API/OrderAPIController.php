<?php
/**
 * File name: OrderAPIController.php
 * Last modified: 2020.06.11 at 16:10:52
 * Author: Farai - https://www.farscorp.co.za/
 * Copyright (c) 2020
 */

namespace App\Http\Controllers\API;


use App\Criteria\Orders\OrdersOfStatusesCriteria;
use App\Criteria\Orders\OrdersOfUserCriteria;
use App\Events\OrderChangedEvent;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Notifications\AssignedOrder;
use App\Notifications\NewOrder;
use App\Notifications\StatusChangedOrder;
use App\Repositories\CartRepository;
use App\Repositories\FoodOrderRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\UserRepository;
use DateTime;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;
use Stripe\Token;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */
class OrderAPIController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;
    /** @var  FoodOrderRepository */
    private $foodOrderRepository;
    /** @var  CartRepository */
    private $cartRepository;
    /** @var  UserRepository */
    private $userRepository;
    /** @var  PaymentRepository */
    private $paymentRepository;
    /** @var  NotificationRepository */
    private $notificationRepository;

    /**
     * OrderAPIController constructor.
     * @param OrderRepository $orderRepo
     * @param FoodOrderRepository $foodOrderRepository
     * @param CartRepository $cartRepo
     * @param PaymentRepository $paymentRepo
     * @param NotificationRepository $notificationRepo
     * @param UserRepository $userRepository
     */
    public function __construct(OrderRepository $orderRepo, FoodOrderRepository $foodOrderRepository, CartRepository $cartRepo, PaymentRepository $paymentRepo, NotificationRepository $notificationRepo, UserRepository $userRepository)
    {
        $this->orderRepository = $orderRepo;
        $this->foodOrderRepository = $foodOrderRepository;
        $this->cartRepository = $cartRepo;
        $this->userRepository = $userRepository;
        $this->paymentRepository = $paymentRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $this->orderRepository->pushCriteria(new RequestCriteria($request));
            $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->orderRepository->pushCriteria(new OrdersOfStatusesCriteria($request));
            $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $orders = $this->orderRepository->all();

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        /** @var Order $order */
        if (!empty($this->orderRepository)) {
            try {
                $this->orderRepository->pushCriteria(new RequestCriteria($request));
                $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            } catch (RepositoryException $e) {
                return $this->sendError($e->getMessage());
            }
            $order = $this->orderRepository->findWithoutFail($id);
        }

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');


    }

    /**
     * Store a newly created Order in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $payment = $request->only('payment');
        if (isset($payment['payment']) && $payment['payment']['method']) {
            if ($payment['payment']['method'] == "Credit Card (PayGenius Gateway)") {
                return $this->stripPayment($request);
            } else {
                return $this->cashPayment($request);

            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    private function stripPayment(Request $request)
    {
        $input = $request->all();
        $amount = 0;
        try {
            $user = $this->userRepository->findWithoutFail($input['user_id']);
            if (empty($user)) {
                return $this->sendError('User not found');
            }
            $stripeToken = Token::create(array(
                "card" => array(
                    "number" => '4242424242424242',
                    "exp_month" => '10',
                    "exp_year" => '2021',
                    "cvc" => $input['stripe_cvc'],
                    "name" => $user->name,
                )
            ));
            if ($stripeToken->created > 0) {
                if (empty($input['delivery_address_id'])) {
                    $order = $this->orderRepository->create(
                        $request->only('user_id', 'order_status_id', 'tax', 'hint')
                    );
                } else {
                    $order = $this->orderRepository->create(
                        $request->only('user_id', 'order_status_id', 'tax', 'delivery_address_id', 'delivery_fee', 'hint')
                    );
                }
                foreach ($input['foods'] as $foodOrder) {
                    $foodOrder['order_id'] = $order->id;
                    $amount += $foodOrder['price'] * $foodOrder['quantity'];
                    $this->foodOrderRepository->create($foodOrder);
                }
                $amount += $order->delivery_fee;
                $amountWithTax = $amount + ($amount * $order->tax / 100);
                $charge = $user->charge((int)($amountWithTax * 100), ['source' => $stripeToken]);
                $payment = $this->paymentRepository->create([
                    "user_id" => $input['user_id'],
                    "description" => trans("lang.payment_order_done"),
                    "price" => $amountWithTax,
                    "status" => $charge->status, // $charge->status
                    "method" => $input['payment']['method'],
                ]);

                $this->orderRepository->update(['payment_id' => $payment->id], $order->id);

                $this->cartRepository->deleteWhere(['user_id' => $order->user_id]);

                Notification::send($order->foodOrders[0]->food->restaurant->users, new NewOrder($order));
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }


        //Call Paygenius keys for proxy payment
        $secret = env("PAYGENIUS_SECRET");
        $token = env("PAYGENIUS_TOKEN");
        $apiCardNumber = $input['stripe_number'];
        $apiCardName =  $user->name;
        $expandDates = DateTime::createFromFormat('y', $input['stripe_exp_year']);
        $expiryYear = $expandDates->format('Y');
        $apiCardExpiryMonth = $input['stripe_exp_month'];
        $apiCVC = $input['stripe_cvc'];
        $apiOrderNo = $order->id;
        $makeTotal = $amountWithTax * 100;
        $apiEmail = $user->email;

        // Based on https://en.wikipedia.org/wiki/Payment_card_number
        // This constant is used in getCardBrand()
        // Note: We're not using regex anymore, with this approach way we can easily read/write/change bin series in this array for future changes
        // Key   (string)           brand, keep it unique in the array
        // Value (array)            for each element in the array:
        // Key   (string)           prefix of card number, minimum 1 digit maximum 6 digits per prefix. You can use "dash" for range. Example: "34" card number starts with 34. Range Example: "34-36" (which means first 6 digits starts with 340000-369999) card number starts with 34, 35 or 36
        // Value (array of strings) valid length of card number. You can set multiple ones. You can also use "dash" for range. Example: "16" means length must be 16 digits. Range Example: "15-17" length must be 15, 16 or 17. Multiple values example: ["12", "15-17"] card number can be 12 or 15 or 16 or 17 digits
        define('CARD_NUMBERS', [
            'american_express' => [
                '34' => ['15'],
                '37' => ['15'],
            ],
            'diners_club' => [
                '36'      => ['14-19'],
                '300-305' => ['16-19'],
                '3095'    => ['16-19'],
                '38-39'   => ['16-19'],
            ],
            'jcb' => [
                '3528-3589' => ['16-19'],
            ],
            'discover' => [
                '6011'          => ['16-19'],
                '622126-622925' => ['16-19'],
                '624000-626999' => ['16-19'],
                '628200-628899' => ['16-19'],
                '64'            => ['16-19'],
                '65'            => ['16-19'],
            ],
            'dankort' => [
                '5019' => ['16'],
                //'4571' => ['16'],// Co-branded with Visa, so it should appear as Visa
            ],
            'maestro' => [
                '6759'   => ['12-19'],
                '676770' => ['12-19'],
                '676774' => ['12-19'],
                '50'     => ['12-19'],
                '56-69'  => ['12-19'],
            ],
            'mastercard' => [
                '2221-2720' => ['16'],
                '51-55'     => ['16'],
            ],
            'unionpay' => [
                '81' => ['16'],// Treated as Discover cards on Discover network
            ],
            'visa' => [
                '4' => ['13-19'],// Including related/partner brands: Dankort, Electron, etc. Note: majority of Visa cards are 16 digits, few old Visa cards may have 13 digits, and Visa is introducing 19 digits cards
            ],
        ]);

        /**
         * Pass card number and it will return brand if found
         * Examples:
         *     get_card_brand('4111111111111111');                    // Output: "visa"
         *     get_card_brand('4111.1111 1111-1111');                 // Output: "visa" function will remove following noises: dot, space and dash
         *     get_card_brand('411111######1111');                    // Output: "visa" function can handle hashed card numbers
         *     get_card_brand('41');                                  // Output: "" because invalid length
         *     get_card_brand('41', false);                           // Output: "visa" because we told function to not validate length
         *     get_card_brand('987', false);                          // Output: "" no match found
         *     get_card_brand('4111 1111 1111 1111 1111 1111');       // Output: "" no match found
         *     get_card_brand('4111 1111 1111 1111 1111 1111', false);// Output: "visa" because we told function to not validate length
         * Implementation Note: This function doesn't use regex, instead it compares digit by digit.
         *                      Because we're not using regex in this function, it's easier to add/edit/delete new bin series to global constant CARD_NUMBERS
         * Performance Note: This function is extremely fast, less than 0.0001 seconds
         * @param  String|Int $cardNumber     (required) Card number to know its brand. Examples: 4111111111111111 or 4111 1111-1111.1111 or 411111###XXX1111
         * @param  Boolean    $validateLength (optional) If true then will check length of the card which must be correct. If false then will not check length of the card. For example you can pass 41 with $validateLength = false still this function will return "visa" correctly
         * @return String                                returns card brand if valid, otherwise returns empty string
         */


        function getCardBrand($cardNumber, $validateLength = true) {
            $foundCardBrand = '';

            $cardNumber = (string)$cardNumber;
            $cardNumber = str_replace(['-', ' ', '.'], '', $cardNumber);// Trim and remove noise

            if(in_array(substr($cardNumber, 0, 1), ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])) {// Try to find card number only if first digit is a number, if not then there is no need to check
                $cardNumber = preg_replace('/[^0-9]/', '0', $cardNumber);// Set all non-digits to zero, like "X" and "#" that maybe used to hide some digits
                $cardNumber = str_pad($cardNumber, 6, '0', STR_PAD_RIGHT);// If $cardNumber passed is less than 6 digits, will append 0s on right to make it 6

                $firstSixDigits   = (int)substr($cardNumber, 0, 6);// Get first 6 digits
                $cardNumberLength = strlen($cardNumber);// Total digits of the card

                foreach(CARD_NUMBERS as $brand => $rows) {
                    foreach($rows as $prefix => $lengths) {
                        $prefix    = (string)$prefix;
                        $prefixMin = 0;
                        $prefixMax = 0;
                        if(strpos($prefix, '-') !== false) {// If "dash" exist in prefix, then this is a range of prefixes
                            $prefixArray = explode('-', $prefix);
                            $prefixMin = (int)str_pad($prefixArray[0], 6, '0', STR_PAD_RIGHT);
                            $prefixMax = (int)str_pad($prefixArray[1], 6, '9', STR_PAD_RIGHT);
                        } else {// This is fixed prefix
                            $prefixMin = (int)str_pad($prefix, 6, '0', STR_PAD_RIGHT);
                            $prefixMax = (int)str_pad($prefix, 6, '9', STR_PAD_RIGHT);
                        }

                        $isValidPrefix = $firstSixDigits >= $prefixMin && $firstSixDigits <= $prefixMax;// Is string starts with the prefix

                        if($isValidPrefix && !$validateLength) {
                            $foundCardBrand = $brand;
                            break 2;// Break from both loops
                        }
                        if($isValidPrefix && $validateLength) {
                            foreach($lengths as $length) {
                                $isValidLength = false;
                                if(strpos($length, '-') !== false) {// If "dash" exist in length, then this is a range of lengths
                                    $lengthArray = explode('-', $length);
                                    $minLength = (int)$lengthArray[0];
                                    $maxLength = (int)$lengthArray[1];
                                    $isValidLength = $cardNumberLength >= $minLength && $cardNumberLength <= $maxLength;
                                } else {// This is fixed length
                                    $isValidLength = $cardNumberLength == (int)$length;
                                }
                                if($isValidLength) {
                                    $foundCardBrand = $brand;
                                    break 3;// Break from all 3 loops
                                }
                            }
                        }
                    }
                }
            }

            return $foundCardBrand;
        }

        $cardNumber = preg_replace("/[^0-9]/", "", $apiCardNumber);
        $apiCardType = getCardBrand($cardNumber);


        //Hash payment request
        $makeEndpoint = env("PAYGENIUS_MAKE_PAYMENT");
        $makePayload = "{\"creditCard\":{\"number\":\"$apiCardNumber\",\"cardHolder\":\"$apiCardName\",\"expiryYear\":$expiryYear,\"expiryMonth\":$apiCardExpiryMonth,\"type\": \"$apiCardType\",\"cvv\":\"$apiCVC\"},\"transaction\":{\"reference\":\"$apiOrderNo\",\"currency\":\"ZAR\",\"amount\":$makeTotal},\"consumer\":{\"name\":\"$apiCardName\",\"surname\":\"$apiCardName\",\"email\":\"$apiEmail\"},\"threeDSecure\":false}";
        $makeSignature = hash_hmac('sha256', trim($makeEndpoint . "\n" . $makePayload), $secret);

        log::info("<===========================================================================================================>");
        Log::info($apiCardNumber);
        log::info($apiCardName);
        log::info($expiryYear);
        Log::info($apiCardExpiryMonth);
        Log::info($apiCardType);
        log::info($apiCVC);
        log::info($apiOrderNo);
        log::info($makeTotal);
        log::info($apiEmail);
        log::info($secret);
        log::info($token);
        log::info($makeSignature);
        log::info($makePayload);
        log::info($makeEndpoint);
        log::info("<============================================================================================================>");

        if ($makeSignature) {

            //Make payment
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $makeEndpoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $makePayload,
                CURLOPT_HTTPHEADER => array(
                    "accept: application/json",
                    "content-type: application/json",
                    "x-signature: " . $makeSignature,
                    "x-token: " . $token
                ),
            ));

            $response = curl_exec($curl);
            $responseArray = json_decode($response, true);
            if($responseArray["success"]) {
                $responseReference = $responseArray["reference"];
                log::info("<=================================================================================> AUTHORIZATION RESPONSE START");
                Log::info($responseReference);
                log::info($responseArray["success"]);
                log::info("<=================================================================================> AUTHORIZATION FIRST RESPONSE END");

            }else{
                echo "Invalid card details supplied => ";
            }


            //close curl connection
            curl_close($curl);

            if(isset($responseReference)){
                //Hash authorization request
                $authEndpoint = env("PAYGENIUS_AUTHORIZE_PAYMENT") . $responseReference . "/execute";
                $authPayload = "{\"reference\":\"$apiOrderNo\",\"currency\":\"ZAR\",\"amount\":$makeTotal,\"source\": \"$apiOrderNo\"}";
                $authSignature = hash_hmac('sha256', trim($authEndpoint . "\n" . $authPayload), $secret);

                //Authorize payment
                $curlauth = curl_init();
                curl_setopt_array($curlauth, array(
                    CURLOPT_URL => $authEndpoint,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => $authPayload,
                    CURLOPT_HTTPHEADER => array(
                        "accept: application/json",
                        "content-type: application/json",
                        "x-signature: " . $authSignature,
                        "x-token: " . $token
                    ),
                ));

                $responseMessage = curl_exec($curlauth);
                //close auth curl connection

                $responseMessageArray = json_decode($responseMessage, true);
                $responseMessageReference = $responseMessageArray["success"];

                curl_close($curlauth);
                if ($responseMessageReference != 0) {
                    return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));

                } else {
                    DB::table('payments')->where('user_id', $input['user_id'])->latest()->limit(1)->update(['status' => 'Card Authorization failed']);
                    $orderResponse = "Payment authorization failed";
                    return $this->sendError($orderResponse);
                    Log::error("Payment authorization failed at => api/v2/payment/XXXXXXXX/execute");
                }

            }else{
                DB::table('payments')->where('user_id', $input['user_id'])->latest()->limit(1)->update(['status' => 'Payment Authorization failed']);
                Log::error("Failed to get transaction authorization from API at => api/v2/payment/XXXXXXXX/execute");
                return $this->sendError('Payment authorization failed');

            }
        }else{
            DB::table('payments')->where('user_id', $input['user_id'])->latest()->limit(1)->update(['status' => 'Signature Authorization failed']);
            Log::error("Make payment signature failed to generate at => api/v2/payment/create");
            return $this->sendError('Make payment signature failed to generate');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    private function cashPayment(Request $request)
    {
        $input = $request->all();
        $amount = 0;
        try {
            $order = $this->orderRepository->create(
                $request->only('user_id', 'order_status_id', 'tax', 'delivery_address_id', 'delivery_fee', 'hint')
            );
            foreach ($input['foods'] as $foodOrder) {
                $foodOrder['order_id'] = $order->id;
                $amount += $foodOrder['price'] * $foodOrder['quantity'];
                $this->foodOrderRepository->create($foodOrder);
            }
            $amount += $order->delivery_fee;
            $amountWithTax = $amount + ($amount * $order->tax / 100);
            $payment = $this->paymentRepository->create([
                "user_id" => $input['user_id'],
                "description" => trans("lang.payment_order_waiting"),
                "price" => $amountWithTax,
                "status" => 'Waiting for Client',
                "method" => $input['payment']['method'],
            ]);

            $this->orderRepository->update(['payment_id' => $payment->id], $order->id);

            $this->cartRepository->deleteWhere(['user_id' => $order->user_id]);

            Notification::send($order->foodOrders[0]->food->restaurant->users, new NewOrder($order));

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $oldOrder = $this->orderRepository->findWithoutFail($id);
        if (empty($oldOrder)) {
            return $this->sendError('Order not found');
        }
        $oldStatus = $oldOrder->payment->status;
        $input = $request->all();

        try {
            $order = $this->orderRepository->update($input, $id);
            if (isset($input['order_status_id']) && $input['order_status_id'] == 5 && !empty($order)) {
                $this->paymentRepository->update(['status' => 'Paid'], $order['payment_id']);
            }
            event(new OrderChangedEvent($oldStatus, $order));

            if (setting('enable_notifications', false)) {
                if (isset($input['order_status_id']) && $input['order_status_id'] != $oldOrder->order_status_id) {
                    Notification::send([$order->user], new StatusChangedOrder($order));
                }

                if (isset($input['driver_id']) && ($input['driver_id'] != $oldOrder['driver_id'])) {
                    $driver = $this->userRepository->findWithoutFail($input['driver_id']);
                    if (!empty($driver)) {
                        Notification::send([$driver], new AssignedOrder($order));
                    }
                }
            }

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

}
