jQuery(document).ready(function ($){
    $("#driver_id").prop('disabled', true);
    $('.confirm-checkbox').on('click',function () {
        let isItChecked = $('.confirm-checkbox').prop('checked');
        if(isItChecked){
            $("#driver_id").prop('disabled', false);
        }else{
            $("#driver_id").prop('disabled', true);
        }
    });
});