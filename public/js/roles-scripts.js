jQuery(document).ready(function ($) {
   let whatIsRole = $("#role-menu").text().trim();
   if(whatIsRole === 'manager'){
      $("#delivery_fee").prop('readonly', true);
   }else{
      $("#delivery_fee").prop('readonly', false);
   }
});